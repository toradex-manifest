# Toradex oe-core Setup

To simplify installation we provide a manifest for the repo tool which manages
the different git repositories and the used versions.
[more on repo](https://code.google.com/p/git-repo/)

This manifest allows setting up Torizon OS or Toradex Reference Images.

## Getting Started and Documentation Torizon OS

- [Step-by-step Getting Started Guide](https://developer.toradex.com/getting-started)
- [Torizon Software Page](https://developer.toradex.com/software/torizon)

## Getting Started and Documentation Reference Images

- [OpenEmbedded (core)](https://developer.toradex.com/knowledge-base/board-support-package/openembedded-(core))
